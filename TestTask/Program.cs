﻿Console.WriteLine("Start");
var rnd = new Random();
const int playersCount = 1_000_000;
var players = Enumerable.Range(0, playersCount).Select(x => new Player
{
    Id = x,
    Name = x + "_name",
    Coins = rnd.Next(0, 5000),
    Cookies = rnd.Next(0, 10)
}).ToList();


Parallel.For(0, 100000, _ =>
{
    var seller = players[rnd.Next(0, playersCount)];
    var buyer = players[rnd.Next(0, playersCount)];
    Player.BuyCookies(seller, buyer, rnd.Next(0, 10), rnd.Next(0, 100));
});

var errorsCount = players.Count(x => x.Coins < 0 || x.Cookies < 0);
Console.WriteLine($"Finish! Errors count: {errorsCount}");

public class Player
{
    public int Id { get; set; }
    public string Name { get; set; }
    public int Coins { get; set; }
    public int Cookies { get; set; }

    public static void BuyCookies(Player buyer, Player seller, int amount, int price)
    {
        if (buyer.Id > seller.Id)
        {
            lock (buyer)
            {
                lock (seller)
                {
                    TrySwapCookies(buyer, seller, amount, price);
                }
            }
        }
        else
        {
            lock (seller)
            {
                lock (buyer)
                {
                    TrySwapCookies(buyer, seller, amount, price);
                }
            }
        }
    }

    private static void TrySwapCookies(Player buyer, Player seller, int amount, int price)
    {
        var totalAmount = price * amount;
        if (buyer.Coins < totalAmount)
        {
            return;// либо кидаем ексепшен
        }

        if (seller.Cookies < amount)
        {
            return;// либо кидаем ексепшен
        }
        
        seller.Cookies -= amount;
        seller.Coins += totalAmount;

        buyer.Cookies += amount;
        buyer.Coins -= totalAmount;
    }
}